package com.example.kotlinspringdemo01

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.web.bind.annotation.*

val animais = mutableListOf<Animal>()

@SpringBootApplication
class KotlinSpringDemo01Application

fun main(args: Array<String>) {
//	animais.add(Animal("Rex", Especie.CACHORRO, 20.0))

	runApplication<KotlinSpringDemo01Application>(*args)
}

@RestController
@RequestMapping("hello")
class HelloWorld{

	@GetMapping()
	fun hello(): String{
		return "Hello world"
	}

	@GetMapping("2")
	fun hello2(): String {
		return "Olá mundo"
	}
}

enum class Especie{ CACHORRO, GATO, BEM_TE_VI }

data class Animal(val nome: String, val especie: Especie, val peso: Double)

@RestController
@RequestMapping("animais")
class AnimalController{

	@GetMapping
	fun index() = animais

	@PostMapping
	fun create(@RequestBody animal: Animal):Animal {
		animais.add(animal)
		return animal
	}
}
